package com.codingdojo.dataprovider;
import java.util.ArrayList;
import java.util.List;

import com.codingdojo.daten.Antwortmoeglichkeit;
import com.codingdojo.daten.Frage;

public class FragebogenParser {

	public static List<Frage> deserialisiere(String questionaire) {
		List<String> fragenMitAntworten = trenneFragen(questionaire);
		return parseFragen(fragenMitAntworten);
	}

	private static List<String> trenneFragen(String questionaire) {
		String[] fragenArray = questionaire.split("\\?");
		List<String> fragenMitAntworten = new ArrayList<>();

		for (String frageBlock : fragenArray) {
			if (!frageBlock.isEmpty()) {
				fragenMitAntworten.add(frageBlock);
			}
		}
		return fragenMitAntworten;
	}

	private static List<Frage> parseFragen(List<String> fragenMitAntworten) {
		List<Frage> fragebogen = new ArrayList<>();

		for (String frageBlock : fragenMitAntworten) {
			fragebogen.add(parseEineFrage(frageBlock));
		}
		return fragebogen;
	}

	private static Frage parseEineFrage(String frageBlock) {

		String[] frageBlockZeilen = frageBlock.split(System.lineSeparator());
		String fragetext = frageBlockZeilen[0] + "?";
		Frage frage = new Frage(fragetext);

		for (int i = 1; i < frageBlockZeilen.length; i++) {
			if (frageBlockZeilen[i].startsWith("*")) {
				frage.addAntwortmoeglichkeit(new Antwortmoeglichkeit(frageBlockZeilen[i].substring(1), true));
			} else {
				frage.addAntwortmoeglichkeit(new Antwortmoeglichkeit(frageBlockZeilen[i], false));
			}
		}
		return frage;
	}

}
