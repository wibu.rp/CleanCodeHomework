package com.codingdojo.ui;

import com.codingdojo.daten.Auswertung;
import com.codingdojo.daten.Korrektur;


public class AuswertungsDialog {

	public void zeige(Auswertung auswertung) {
		String richtigText = formatiereRichtigeAntworten(auswertung);
		String auswertungsText = formatiereKorrekturen(auswertung);
		
		gibAuswertungAus(richtigText, auswertungsText);
	}


	protected String formatiereRichtigeAntworten(Auswertung auswertung) {
		StringBuilder richtigText = new StringBuilder();
		richtigText.append("Auswertung:");
		richtigText.append(System.lineSeparator());
		richtigText.append(auswertung.getKorrekteAntworten());
		richtigText.append(" / ");
		richtigText.append(auswertung.getAnzahlFragen());
		richtigText.append(" (");
		richtigText.append(auswertung.berechneProzent());
		richtigText.append("%)");
		richtigText.append(" wurden richtig beantwortet.");
		return richtigText.toString();
	}
	
	protected String formatiereKorrekturen(Auswertung auswertung) {
		StringBuilder formatter = new StringBuilder();
		
		for (Korrektur korrektur : auswertung.getKorrekturen()) {
			formatter.append(formatiereKorrektur(korrektur));
			formatter.append(System.lineSeparator());
		}
		return formatter.toString();
	}
	
	private String formatiereKorrektur(Korrektur korrektur) {
		
		return korrektur.getFrageText()
				+ System.lineSeparator()
				+ "Falsch, Du hast geantwortet: "
				+ String.join(", ", korrektur.getGegebeneAntworten())
				+ System.lineSeparator()
				+ "Richtig waere: "
				+ String.join(", ", korrektur.getRichtigeAntworten())
				+ System.lineSeparator();

	}
	
	private void gibAuswertungAus(String richtigText, String auswertungsText) {
		System.out.println(richtigText);
		System.out.println(System.lineSeparator());
		System.out.println(auswertungsText);
	}
}
