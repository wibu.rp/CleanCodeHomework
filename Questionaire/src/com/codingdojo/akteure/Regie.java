package com.codingdojo.akteure;
import java.io.IOException;
import java.util.List;

import com.codingdojo.dataprovider.FragebogenFundgrube;
import com.codingdojo.dataprovider.FragebogenParser;
import com.codingdojo.dataprovider.fileio.FileIO;
import com.codingdojo.daten.Aufgabe;
import com.codingdojo.daten.Frage;
import com.codingdojo.ui.Aufgabendialog;

public class Regie {

	public void quizStarten() throws IOException {
		FragebogenFundgrube fundgrube = new FragebogenFundgrube();
		Quizmaster quizmaster = new Quizmaster();
		
		List<Frage> fragebogen = fundgrube.holeFragebogen();
		quizmaster.quizDurchfuehren(fragebogen);
	}

	
}
