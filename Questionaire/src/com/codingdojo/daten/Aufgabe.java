package com.codingdojo.daten;

import java.util.List;

public class Aufgabe {

	private int i;
	private int n;
	private String frage;
	private List<Antwortmoeglichkeit> antwortMoeglichkeiten;

	public Aufgabe(String frageText, List<Antwortmoeglichkeit> antwortMoeglichkeiten, int fragenIndex,
			int anzahlFragen) {
		this.frage = frageText;
		this.antwortMoeglichkeiten = antwortMoeglichkeiten;
		this.i = fragenIndex;
		this.n = anzahlFragen;
	}

	public int getAufgabenIndex() {
		return i;
	}

	public int getAnzahlAufgaben() {
		return n;
	}

	public String getFrageText() {
		return frage;
	}

	public List<Antwortmoeglichkeit> getAntwortMoeglichkeiten() {
		return antwortMoeglichkeiten;
	}

}
