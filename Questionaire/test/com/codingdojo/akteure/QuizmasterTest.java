package com.codingdojo.akteure;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

import com.codingdojo.daten.Antwortmoeglichkeit;
import com.codingdojo.daten.Auswertung;
import com.codingdojo.daten.Frage;
import com.codingdojo.daten.Korrektur;

public class QuizmasterTest {

	
	
	@Test
	public void testeAuswerten(){
		
		List<Frage> fragebogen = createFragebogen();
		
		QuizmasterMock quizzmaster = new QuizmasterMock();
		LinkedHashMap<Frage, Set<Integer>> beantworteteFragen = new LinkedHashMap<>();
		beantworteteFragen.put(fragebogen.get(0), new TreeSet<>(Arrays.asList(2)));
		beantworteteFragen.put(fragebogen.get(1), new TreeSet<>(Arrays.asList(1)));
		beantworteteFragen.put(fragebogen.get(2), new TreeSet<>(Arrays.asList(1,0)));
		beantworteteFragen.put(fragebogen.get(3), new TreeSet<>(Arrays.asList(1,2)));
				
		quizzmaster.setBeantworteteFragen(beantworteteFragen);
		
		Auswertung auswertung = quizzmaster.auswerten();
		
	 	assertEquals(2, auswertung.getKorrekteAntworten());
	 	assertEquals(2, auswertung.getKorrekturen().size());
	 	
	 	Korrektur actualkorrektur = auswertung.getKorrekturen().get(0);
	 	
	 	assertEquals(fragebogen.get(0).getFragetext(), actualkorrektur.getFrageText());
	 	assertEquals(Arrays.asList("a"), actualkorrektur.getRichtigeAntworten());
	 	
	 	Korrektur secondKorrektur = auswertung.getKorrekturen().get(1);
	 	
	 	assertEquals(fragebogen.get(3).getFragetext(), secondKorrektur.getFrageText());
	 	assertEquals(Arrays.asList("Elefant", "Ottifant"), secondKorrektur.getRichtigeAntworten());
	}
	
	
	private List<Frage> createFragebogen() {
		Frage frage1 = new Frage("Frage1?");
		frage1.addAntwortmoeglichkeit(new Antwortmoeglichkeit("a", true));
		frage1.addAntwortmoeglichkeit(new Antwortmoeglichkeit("b", false));
		frage1.addAntwortmoeglichkeit(new Antwortmoeglichkeit("c", false));

		Frage frage2 = new Frage("Frage2?");
		frage2.addAntwortmoeglichkeit(new Antwortmoeglichkeit("d", false));
		frage2.addAntwortmoeglichkeit(new Antwortmoeglichkeit("e", true));
		frage2.addAntwortmoeglichkeit(new Antwortmoeglichkeit("f", false));

		Frage frage3 = new Frage("Auf was ist Frieda allergisch?");
		frage3.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Gras", true));
		frage3.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Gr�nes Gras", true));
		frage3.addAntwortmoeglichkeit(new Antwortmoeglichkeit("n�x", false));
		frage3.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Birke", false));

		Frage frage4 = new Frage("Welche Tiere mag Frieda?");
		frage4.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Elefant", true));
		frage4.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Ottifant", true));
		frage4.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Spinnen", false));
		frage4.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Birke", false));

		List<Frage> fragebogen = new ArrayList<>();
		fragebogen.add(frage1);
		fragebogen.add(frage2);
		fragebogen.add(frage3);
		fragebogen.add(frage4);

		return fragebogen;
	}
	
	

}
