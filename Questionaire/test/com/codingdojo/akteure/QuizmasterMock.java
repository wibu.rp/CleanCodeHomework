package com.codingdojo.akteure;

import java.util.LinkedHashMap;
import java.util.Set;

import com.codingdojo.daten.Frage;

public class QuizmasterMock extends Quizmaster {

	public QuizmasterMock() {
		super();
	}

	public void setBeantworteteFragen(LinkedHashMap<Frage,Set<Integer>> beantworteteFragen) {
		super.beantworteteFragen = beantworteteFragen;
	}
}
