package com.codingdojo.daten;

import static org.junit.Assert.*;

import org.junit.Test;

public class AuswertungTest {

	@Test
	public void testBerechneProzent() {
		int actualProzent = berechneProzent(2, 1);
		assertEquals(50, actualProzent);

		actualProzent = berechneProzent(2, 2);
		assertEquals(100, actualProzent);

		actualProzent = berechneProzent(2, 0);
		assertEquals(0, actualProzent);

		actualProzent = berechneProzent(3, 1);
		assertEquals(33, actualProzent);

		actualProzent = berechneProzent(3, 2);
		assertEquals(66, actualProzent);
	}

	private int berechneProzent(int anzahlFragen, int anzahlKorrekteAntworten) {
		AuswertungsMock auswertung = new AuswertungsMock(anzahlFragen, anzahlKorrekteAntworten);
		return auswertung.berechneProzent();
	}

}

