package com.codingdojo.ui;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

import com.codingdojo.daten.Antwortmoeglichkeit;
import com.codingdojo.daten.Auswertung;
import com.codingdojo.daten.AuswertungsMock;
import com.codingdojo.daten.Frage;

public class AuswertungsDialogTest {

	@Test
	public void testFormatiereRichtigeAntworten(){
		AuswertungsDialog auswertungsDialog = new AuswertungsDialog();
		AuswertungsMock auswertung = new AuswertungsMock(2, 1);
		String actual = auswertungsDialog.formatiereRichtigeAntworten(auswertung);
		String expected = "Auswertung:" + System.lineSeparator() 
		+ "1 / 2 (50%) wurden richtig beantwortet.";
		assertEquals(expected, actual);
	}
	
	@Test
	public void testFormatiereKorrekturen(){
		AuswertungsDialog auswertungsDialog = new AuswertungsDialog();
		Auswertung auswertung = new Auswertung(3);
		Set<Integer> antwortset = new TreeSet<Integer>();
		antwortset.add(2);
		Frage frage = new Frage ("Frage?");
		frage.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Richtige Antwort", true));
		frage.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Weitere Antwort", false));
		auswertung.addKorrektur(frage, antwortset);
		
		Frage frage2 = new Frage("Noch eine Frage?");
		frage2.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Noch eine Richtige Antwort", true));
		auswertung.addKorrektur(frage2, antwortset);
		String actual = auswertungsDialog.formatiereKorrekturen(auswertung);
		String expected = "Frage?" + System.lineSeparator() 
				+ "Falsch, Du hast geantwortet: keine Ahnung" + System.lineSeparator()
				+ "Richtig waere: " 
				+ "Richtige Antwort" + System.lineSeparator() + System.lineSeparator()
				+ "Noch eine Frage?" + System.lineSeparator() 
				+ "Falsch, Du hast geantwortet: keine Ahnung" + System.lineSeparator()
				+ "Richtig waere: " 
				+ "Noch eine Richtige Antwort" + System.lineSeparator() + System.lineSeparator();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testFormatiereKorrekturenMultipleChoice() {
		AuswertungsDialog auswertungsDialog = new AuswertungsDialog();
		Auswertung auswertung = new Auswertung(3);
		Set<Integer> antwortset = new TreeSet<Integer>();
		antwortset.add(1);
		Frage frage = new Frage ("Frage?");
		frage.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Richtige Antwort", true));
		frage.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Weitere Antwort", true));
		auswertung.addKorrektur(frage, antwortset);
		
		Frage frage2 = new Frage("Noch eine Frage?");
		frage2.addAntwortmoeglichkeit(new Antwortmoeglichkeit("Noch eine Richtige Antwort", true));
		auswertung.addKorrektur(frage2, antwortset);
		String actual = auswertungsDialog.formatiereKorrekturen(auswertung);
		String expected = "Frage?" + System.lineSeparator() 
				+ "Falsch, Du hast geantwortet: Weitere Antwort" + System.lineSeparator()
				+ "Richtig waere: " 
				+ "Richtige Antwort, Weitere Antwort" + System.lineSeparator() + System.lineSeparator()
				+ "Noch eine Frage?" + System.lineSeparator() 
				+ "Falsch, Du hast geantwortet: keine Ahnung" + System.lineSeparator()
				+ "Richtig waere: " 
				+ "Noch eine Richtige Antwort" + System.lineSeparator() + System.lineSeparator();
		assertEquals(expected, actual);
	}
}
