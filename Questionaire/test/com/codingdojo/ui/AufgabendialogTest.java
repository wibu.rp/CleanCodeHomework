package com.codingdojo.ui;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Test;

import com.codingdojo.daten.Antwortmoeglichkeit;
import com.codingdojo.daten.Aufgabe;

public class AufgabendialogTest {

	@Test
	public void testErstelleDialogtext() {
		String fragetest = "Frage1?";
		List<Antwortmoeglichkeit> antworten = new ArrayList<>();
		antworten.add(new Antwortmoeglichkeit("a", false));
		antworten.add(new Antwortmoeglichkeit("b", true));

		Aufgabe aufgabe = new Aufgabe(fragetest, antworten, 1, 5);
		Aufgabendialog aufgabendialog = new Aufgabendialog();
		String tatsaechlich = aufgabendialog.erstelleDialogtext(aufgabe);

		String erwartet = "Frage 1 von 5" + System.lineSeparator()
				+ "Frage1?" + System.lineSeparator() 
				+ "1) a" 
				+ System.lineSeparator() 
				+ "2) b" 
				+ System.lineSeparator()
				+ "3) Keine Ahnung";

		assertEquals(erwartet, tatsaechlich);
	}

}

